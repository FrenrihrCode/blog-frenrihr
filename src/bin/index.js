const fetch = require('node-fetch');
const fs = require('fs');
let postsObj = require('../routes/blog/_post.json');

require('dotenv').config();

const limit = 10;
const API = process.env.GHOST_API;
//const blogTitle = process.env.BLOG_TITLE;
const blogUrl = process.env.BLOG_URL;
//const blogCover = process.env.BLOG_COVER;
//const blogDesc = process.env.BLOG_DESC;
//const blogFavicon = process.env.BLOG_FAVICON;

const getDate = (date) => {
    if (date) {
        return new Date(date).toISOString();
    }
    return new Date().toISOString();
}

//crear un sitemap para SEO
const createSitemap = async (data) => {
    const parseItems = await data.map((item) => {
        return `
        <url>
            <loc>${blogUrl}/blog/${item.slug}</loc>
            <lastmod>${getDate(item.createdAt)}</lastmod>
            <priority>0.8</priority>
        </url>
        `;
    }).join('');

    const template = `
    <?xml version="1.0" encoding="UTF-8"?>

    <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
        <url>
            <loc>${blogUrl}</loc>
            <lastmod>${getDate()}</lastmod>
            <priority>1.0</priority>
        </url>
        <url>
            <loc>${blogUrl}/about</loc>
            <lastmod>${getDate()}</lastmod>
            <priority>0.8</priority>
        </url>
        <url>
            <loc>${blogUrl}/contact</loc>
            <lastmod>${getDate()}</lastmod>
            <priority>0.8</priority>
        </url>
        <url>
            <loc>${blogUrl}/proyectos</loc>
            <lastmod>${getDate()}</lastmod>
            <priority>0.8</priority>
        </url>
        <url>
            <loc>${blogUrl}/tutoriales</loc>
            <lastmod>${getDate()}</lastmod>
            <priority>0.8</priority>
        </url>
        ${parseItems}
    </urlset> 
    `.trim();

    return template;
}

const writefilePost = async (obj) => {
    const siteMap = await createSitemap(obj);
    const parseData = JSON.stringify(obj);
    fs.writeFileSync('./src/routes/blog/_post.json', parseData);
    console.log('Datos actualizados!');
    fs.writeFileSync('./static/sitemap.xml', siteMap);
    console.log('Sitemap actualizado!');
}
/* const createRss = () => {
    const template = `
    <?xml version="1.0" encoding="UTF-8" ?>
    <rss version="2.0">

    <channel>
        <title><![CDATA[${blogTitle}]]></title>
        <description><![CDATA[${blogDesc}]]></description>
        <image>
            <url>${blogFavicon}</url>
            <title><![CDATA[${blogTitle}]]></title>
            <link><![CDATA[${blogUrl}]]></link>
        </image>
        <generator>FrenrihrBlog</generator>
    </channel>

    </rss>
    `;
} */

const fetchData = async () => {
    const response = await fetch(`${API}&limit=${limit}`);
    const data = await response.json();

    const posts = await data.posts.map(post => ({
        title: post.title,
        html: post.html,
        slug: post.slug,
        createdAt: post.created_at,
        id: post.id,
        desc: post.excerpt,
        tag: post.primary_tag.slug,
        image: post.feature_image,
    }));

    if (postsObj.length >= limit) {
        if (posts[0].title === postsObj[0].title) {
            postsObj.shift();
            postsObj.unshift(posts[0]);
            writefilePost(postsObj);
        } else {
            postsObj.unshift(posts[0]);
            writefilePost(postsObj);
        }
    } else {
        writefilePost(posts);
    }
}

fetchData();
const emojiTag = (name) => {
    const emojis = {
        tutoriales: '🚀',
        movil: '📱',
        proyectos: '🔨',
        general: '📌',
        documentacion: '📝'
    };
    return emojis[name];
};

export default emojiTag;

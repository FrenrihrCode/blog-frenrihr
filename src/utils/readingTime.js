const readingTime = (text) => {
    const wordsPorMinute = 200;
    const numberWords = text.split(/\s/g).length;
    const minutes = numberWords/wordsPorMinute;
    const readTime = Math.ceil(minutes);
    return `📰 ${readTime} Min.`;
}

export default readingTime;

const formatIsoTime = (ISOTime) => {
    let date = new Date(ISOTime);
    const options = { year: 'numeric', month: 'long', day: 'numeric' };
    return date.toLocaleDateString('es-ES', options);
};

export default formatIsoTime;
const analitycsTracking = (event) => {
    if (typeof window != 'undefined') {
        /*  window.gtag('send', 'event', 'social', 'click', event, {
             nonInteraction: true
         }); */
        window.gtag('event', 'click', {
            'event_category': 'social',
            'event_label': event,
            'non_interaction': true
        });
    }
};

export default analitycsTracking;